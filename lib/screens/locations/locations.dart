import 'dart:async';
import 'package:flutter/material.dart';
import '../../components/default_app_bar.dart';
import '../../components/image_banner.dart';
import '../../components/location_tile.dart';
import '../../models/location.dart';
import '../../styles.dart';
import '../location_detail/location_detail.dart';

const ListItemHeight = 245.0;

class Locations extends StatefulWidget {
  @override
  createState() => _LocationsState();
}

class _LocationsState extends State<Locations> {
  List<Location> locations = [];
  bool loading = false;

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: DefaultAppBar(),
        body: RefreshIndicator(
            onRefresh: loadData,
            child: Column(children: [
              _renderProgressBar(context),
              Expanded(child: _renderListView(context)),
            ])));
  }

  Future<void> loadData() async {
    setState(() => this.loading = true);

    final locations = await Location.fetchAll();

    setState(() {
      this.locations = locations;
      this.loading = false;
    });
  }

  Widget _renderProgressBar(BuildContext context) {
    return (this.loading
        ? LinearProgressIndicator(
            value: null,
            backgroundColor: Colors.white,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.grey))
        : Container());
  }

  Widget _renderListView(BuildContext context) {
    return ListView(
      children: _locationsList(context, this.locations),
    );
  }

  List<GestureDetector> _locationsList(
      BuildContext context, List<Location> locations) {
    return locations
        .map((location) => GestureDetector(
              child: _locationListTile(location),
              onTap: () => _navigateToLocationDetail(context, location.id),
            ))
        .toList();
  }

  Widget _locationListTile(Location location) {
    return Container(
      height: ListItemHeight,
      child: Stack(
        children: [
          ImageBanner(url: location.url, height: ListItemHeight),
          _tileFooter(location),
        ],
      ),
    );
  }

  Widget _tileFooter(Location location) {
    final info = LocationTile(location: location, darkTheme: true);
    final overlay = Container(
      padding: EdgeInsets.symmetric(
          vertical: 5.0, horizontal: Styles.horizontalPaddingDefault),
      decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
      child: info,
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [overlay],
    );
  }

  _navigateToLocationDetail(BuildContext context, int locationID) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => LocationDetail(locationID)));
  }
}
