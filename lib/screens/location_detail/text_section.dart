import 'package:flutter/material.dart';
import 'package:test_app/styles.dart';

class TextSection extends StatelessWidget {
  final String _title;
  final String _body;

  TextSection(this._title, this._body);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(Styles.horizontalPaddingDefault, 25.0,
                Styles.horizontalPaddingDefault, 0.0),
            child: Text(_title.toUpperCase(),
                textAlign: TextAlign.left, style: Styles.headerLarge),
          ),
          Container(
            padding: EdgeInsets.symmetric(
                vertical: 10.0, horizontal: Styles.horizontalPaddingDefault),
            child: Text(_body, style: Styles.textDefault),
          ),
        ]);
  }
}
