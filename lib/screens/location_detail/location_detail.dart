import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../components/default_app_bar.dart';
import '../../components/location_tile.dart';
import '../../models/location.dart';
import '../../styles.dart';
import '../../components/image_banner.dart';
import 'text_section.dart';

const BannerImageHeight = 250.0;
const BodyVerticalPadding = 20.0;
const FooterHeight = 100.0;

class LocationDetail extends StatefulWidget {
  final int _locationID;

  LocationDetail(this._locationID);

  @override
  createState() => _LocationDetailState(this._locationID);
}

class _LocationDetailState extends State<LocationDetail> {
  final int _locationID;
  Location location = Location.blank();

  _LocationDetailState(this._locationID);

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() async {
    final location = await Location.fetchByID(_locationID);

    if (mounted) {
      setState(() {
        this.location = location;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: DefaultAppBar(),
        body: Stack(
          children: [
            _renderBody(),
            _renderFooter(),
          ],
        ));
  }

  Widget _renderBody() {
    var result = List<Widget>();
    result.add(ImageBanner(url: this.location.url, height: BannerImageHeight));
    result.add(_renderHeader());
    result.addAll(_textSections(this.location));
    result.add(Container(height: FooterHeight));

    return SingleChildScrollView(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: result,
    ));
  }

  Widget _renderHeader() {
    return Container(
        padding: EdgeInsets.fromLTRB(Styles.horizontalPaddingDefault,
            BodyVerticalPadding, Styles.horizontalPaddingDefault, 0.0),
        child: LocationTile(location: this.location, darkTheme: false));
  }

  List<Widget> _textSections(Location location) {
    return location.facts
        .map((fact) => TextSection(fact.title, fact.text))
        .toList();
  }

  Widget _renderFooter() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          decoration: BoxDecoration(color: Colors.white.withOpacity(0.5)),
          height: FooterHeight,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
            child: _renderBookButton(),
          ),
        )
      ],
    );
  }

  Widget _renderBookButton() {
    return FlatButton(
      color: Styles.accentColor,
      textColor: Styles.textColorBright,
      onPressed: _handleBookPress,
      child: Text('Book'.toUpperCase(), style: Styles.textCTAButton),
    );
  }

  void _handleBookPress() async {
    const url = 'mailto: hello@tourism.co?subject=inquiry';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
