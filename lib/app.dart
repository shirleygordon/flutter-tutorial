import 'package:flutter/material.dart';
import 'screens/locations/locations.dart';
import 'styles.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Locations(),
      theme: _theme(),
    );
  }

  ThemeData _theme() {
    return ThemeData(
      appBarTheme: AppBarTheme(
        textTheme: TextTheme(headline6: Styles.navBarTitle),
      ),
      textTheme: TextTheme(
        headline6: Styles.headerLarge,
        bodyText2: Styles.textDefault,
      ),
    );
  }
}
