import 'package:flutter/material.dart';

class ImageBanner extends StatelessWidget {
  final String url;
  final double height;

  ImageBanner({this.url, this.height});

  @override
  Widget build(BuildContext context) {
    Image image;
    try {
      if (url.isNotEmpty) {
        image = Image.network(url, fit: BoxFit.cover);
      }
    } catch (e) {
      print('Could not load image $url');
    }
    return Container(
      constraints: BoxConstraints.expand(
        height: height,
      ),
      decoration: BoxDecoration(color: Colors.grey),
      child: image,
    );
  }
}
