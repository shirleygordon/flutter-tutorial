import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:json_annotation/json_annotation.dart';
import 'location_fact.dart';
import '../endpoint.dart';

part 'location.g.dart';

@JsonSerializable()
class Location {
  final int id;
  final String name;
  final String url;
  final String userItinerarySummary;
  final String tourPackageName;
  final List<LocationFact> facts;
  static Map<String, String> locationUrls = {
    'Arashiyama Bamboo Grove':
        'https://cdn-images-1.medium.com/max/2000/1*vdJuSUKWl_SA9Lp-32ebnA.jpeg',
    'Mount Fuji':
        'https://rimage.gnst.jp/livejapan.com/public/article/detail/a/00/02/a0002587/img/basic/a0002587_main.jpg',
    'Kiyomizu-dera':
        'https://gaijinpot.scdn3.secure.raxcdn.com/app/uploads/sites/6/2017/07/Kiyomizudera-Temple-Kyoto.jpg',
    'Kinkaku-ji':
        'https://media-cdn.tripadvisor.com/media/photo-s/16/72/0f/2c/caption.jpg',
    'Odaiba':
        'https://rimage.gnst.jp/livejapan.com/public/article/detail/a/00/02/a0002644/img/basic/a0002644_main.jpg'
  };

  Location(
      {this.id,
      this.name,
      this.url,
      this.userItinerarySummary,
      this.tourPackageName,
      this.facts});

  Location.blank()
      : id = 0,
        name = '',
        url = '',
        userItinerarySummary = '',
        tourPackageName = '',
        facts = [];

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);

  /// Fetches all the locations from the webserver.
  /// and returns a [List] of [Location]s.
  static Future<List<Location>> fetchAll() async {
    var uri = Endpoint.uri('/locations');

    // Get the locations data from the server.
    final resp = await http.get(uri.toString());

    // Throw exception if the status code is invalid.
    if (resp.statusCode != 200) {
      throw (resp.body);
    }

    // Convert the response body to a list of locations.
    List<Location> list = new List<Location>();
    Map<String, dynamic> jsonMap = json.decode(resp.body);
    jsonMap.forEach((key, value) {
      // Replacing the url from the server with a new url for the pictures,
      // as for some reason the urls from the server are invalid.
      value['url'] = locationUrls[value['name']];
      value['userItinerarySummary'] = 'Day 1: 2PM - 3:30PM';
      value['tourPackageName'] = 'Standard Package';
      list.add(Location.fromJson(value));
    });

    return list;
  }

  /// Fetches a specific location from the server by [id]
  /// and returns a [Location].
  static Future<Location> fetchByID(int id) async {
    var uri = Endpoint.uri('/locations/$id');

    // Get the location's data from the server.
    final resp = await http.get(uri.toString());

    // Throw exception if the status code is invalid.
    if (resp.statusCode != 200) {
      throw (resp.body);
    }

    // Convert the response body to a Location object.
    final Map<String, dynamic> itemMap = json.decode(resp.body);

    // Replacing the url from the server with a new url for the pictures,
    // as for some reason the urls from the server are invalid.
    itemMap['url'] = locationUrls[itemMap['name']];
    itemMap['userItinerarySummary'] = 'Day 1: 2PM - 3:30PM';
    itemMap['tourPackageName'] = 'Standard Package';
    return Location.fromJson(itemMap);
  }
}
