import '../models/location.dart';
import '../models/location_fact.dart';

class MockLocation extends Location {
  static List<Location> fetchAll() {
    return [
      Location(
          id: 1,
          name: 'Kiyomizu-dera',
          url:
              'https://raw.githubusercontent.com/seenickcode/tourismandco/step/step07/assets/images/kiyomizu-dera.jpg',
          facts: <LocationFact>[
            LocationFact(
                title: 'Summary',
                text:
                    'Kiyomizu-dera, officially Otowa-san Kiyomizu-dera, is an independent Buddhist temple in eastern Kyoto. The temple is part of the Historic Monuments of Ancient Kyoto UNESCO World Heritage site.'),
            LocationFact(
                title: 'Architectural Style',
                text: 'Japanese Buddhist architecture.'),
          ]),
      Location(
          id: 2,
          name: 'Mount Fuji',
          url:
              'https://raw.githubusercontent.com/seenickcode/tourismandco/step/step07/assets/images/fuji.jpg',
          facts: <LocationFact>[
            LocationFact(
                title: 'Summary',
                text:
                    'Japan’s Mt. Fuji is an active volcano about 100 kilometers southwest of Tokyo. Commonly called “Fuji-san,” it’s the country’s tallest peak, at 3,776 meters. A pilgrimage site for centuries, it’s considered one of Japan’s 3 sacred mountains, and summit hikes remain a popular activity. Its iconic profile is the subject of numerous works of art, notably Edo Period prints by Hokusai and Hiroshige.'),
            LocationFact(
                title: 'Did You Know',
                text:
                    'There are three cities that surround Mount Fuji: Gotemba, Fujiyoshida and Fujinomiya.'),
          ]),
      Location(
          id: 3,
          name: 'Arashiyama Bamboo Grove',
          url:
              'https://raw.githubusercontent.com/seenickcode/tourismandco/step/step07/assets/images/arashiyama.jpg',
          facts: <LocationFact>[
            LocationFact(
                title: 'Summary',
                text:
                    'While we could go on about the ethereal glow and seemingly endless heights of this bamboo grove on the outskirts of Kyoto, the sight\'s pleasures extend beyond the visual realm.'),
            LocationFact(
                title: 'How to Get There',
                text:
                    'Kyoto airport, with several terminals, is located 16 kilometres south of the city and is also known as Kyoto. Kyoto can also be reached by transport links from other regional airports.'),
          ]),
    ];
  }

  static Location fetchByID(int locationID) {
    List<Location> locations = fetchAll();
    for (var i = 0; i < locations.length; i++) {
      if (locations[i].id == locationID) {
        return locations[i];
      }
    }
    return null;
  }

  static Location fetchAny() {
    return MockLocation.fetchAll().first;
  }
}
